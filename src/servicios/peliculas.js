import axios from 'axios';
import global from '../../global';

export default {

   async getmovies(){

        try {
            const url = global.BaseURL+'discover/movie?sort_by=popularity.desc&api_key='+ global.ApiKey;
            let result = await axios.get(url);
            if(result && result.status == 200) {
                return result.data;
            }
        } catch (error) {
            return error.response.data;
        }
    },

   async  getmoviereco()
    {
        try {
            const idreco = '611291';
            const url = global.BaseURL+'movie/'+ idreco +'/recommendations?api_key='+ global.ApiKey+ ''
            let result = await axios.get(url);
            if(result && result.status == 200) {
                return result.data;
            }
        } catch (error) {
            return error.response.data;
        }
    },

    async getmovierated()
    {
        try {
          const url = global.BaseURL+'movie/top_rated?api_key='+ global.ApiKey+ '';
          let result = await axios.get(url);
          if(result && result.status == 200) {
              return result.data;
          }
          
      } catch (error) {
        return error.response.data;
      }

    },

    async getmoviedetail(id)
    {
      
    try {
      const idmovie= `${id}?api_key=`;
      const url =  global.BaseURL+'movie/'+ idmovie + global.ApiKey+'&append_to_response=overview';
      let result = await axios.get(url);
      if(result && result.status == 200) {
          return result.data;
      }
    } catch (error) {
        return error.response.data;
    }

    },

    async gettvfavorite()
    {
    try {
        const url =  global.BaseURL+'tv/popular?api_key='+ global.ApiKey+ ''
        let result = await axios.get(url);
        if(result && result.status == 200) {
            return result.data;
        }
    } catch (error) {
        return error.response.data;
    }

    },

    async gettvrecommended()
    {
      const idtvreco = '100';
      const url =  global.BaseURL+'tv/'+idtvreco+ '/recommendations?api_key='+ global.ApiKey;
      try {
        let result = await axios.get(url);
        if(result && result.status == 200) {
            return result.data;
        }
      } catch (error) {
        return error.response.data;
      }

    },

    async gettvrated(){
      
        const url =  global.BaseURL+'tv/'+'top_rated?api_key='+ global.ApiKey + '';
        try {
            let result = await axios.get(url);
            if(result && result.status == 200) {
                return result.data;
            }
        } catch (error) {
            return error.response.data;
        }
    },

    async gettvdetail(id){
        const idtvdetail = id;
        const url =  global.BaseURL+'tv/'+ idtvdetail+'?api_key='+ global.ApiKey + '&append_to_response=overview';
        try {
            let result = await axios.get(url);
            if(result && result.status == 200) {
                return result.data;
            }
        } catch (error) {
            return error.response.data;
        }
      },

}